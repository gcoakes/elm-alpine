FROM alpine
ARG ELM_DL_URL=https://github.com/elm/compiler/releases/download/0.19.1/binary-for-linux-64-bit.gz
RUN wget -qO- "$ELM_DL_URL" | gunzip > /bin/elm && chmod +x /bin/elm
